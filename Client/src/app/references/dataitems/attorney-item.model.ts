export interface AttorneyItem {
    'attorneyId': number;
    'attorneyName': string;
    'address1': string;
    'address2': string;
    'city': string;
    'stateName': string;
    'postalCode': string;
    'phoneNumber': string;
    'faxNumber': string;
    'emailAddress':string;
    'modifiedBy': string;
}
