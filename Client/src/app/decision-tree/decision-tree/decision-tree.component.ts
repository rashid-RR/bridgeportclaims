import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-decision-tree',
  template: '<router-outlet></router-outlet>',
  styleUrls: ['./decision-tree.component.css']
})
export class DecisionTreeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
