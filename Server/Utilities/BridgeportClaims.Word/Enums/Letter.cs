﻿namespace BridgeportClaims.Word.Enums
{
    public enum LetterType
    {
        Ime = 0,
        BenExhaust = 1,
        PipApp = 2,
        Denial = 3,
        UnderInvestigation = 4,
        DrNoteLetter = 5
    }
}