﻿namespace LakerFileImporter.StringConstants
{
    public static class Constants
    {
        public const string DbConnStrName = "BridgeportClaimsConnectionString";
        public const string AppIsInDebugMode = "ApplicationIsInDebugMode";
        public const string LakerFilePathKey = "LakerFilePath";
        public const string FileProcessorTopNumberKey = "FileProcessorTopNumber";
        public const string LakerFileTypeName = "Laker Import";
        public const string FileUploadApiHostNameKey = "FileUploadApiHostName";
        public const string FileUploadApiUrlKey = "FileUploadApiUrl";
        public const string AuthenticationApiUrlKey = "AuthenticationApiUrl";
        public const string AuthenticationUserNameKey = "AuthenticationUserName";
        public const string AuthenticationPasswordKey = "AuthenticationPassword";
        public const string LakerFileProcessingApiUrlKey = "LakerFileProcessingApiUrl";
        public const string MonthFolderFormatKey = "MonthFolderFormat";
        public const string SftpHostKey = "SftpHost";
        public const string SftpUserNameKey = "SftpUserName";
        public const string SftpPasswordKey = "SftpPassword";
        public const string SftpRemoteSitePathKey = "SftpRemoteSitePath";
        public const string SftpPortKey = "SftpPort";
        public const string ProcessSftpKey = "ProcessSftp";
    }
}